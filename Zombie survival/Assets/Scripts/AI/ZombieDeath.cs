﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieDeath : MonoBehaviour
{
    public int EnemyHealth = 20;
    public GameObject TheEnemy;

    void DamageZombie   (int DamageAmount)
    {
        EnemyHealth -= DamageAmount;
    }
    // Update is called once per frame
    void Update()
    {
        if(EnemyHealth <= 0 )
        {
           // TheEnemy.GetComponent<Animator>().Play("Melee_OneHanded");
            TheEnemy.SetActive(false);
        }
    }
}
