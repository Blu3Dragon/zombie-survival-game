﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class EnemyFollow : MonoBehaviour
{
    public float FollowRadius = 10f;
    public float hearRadius = 15f;
    public float attackRadius = 2f;
    public Transform target;
    public float speed = 3f;
    float wait = 0f;
    public bool isAttacking = false;
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, FollowRadius);

        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, hearRadius);

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, attackRadius);
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        float distance = Vector3.Distance(target.position, transform.position);
        if (distance <= FollowRadius && distance > attackRadius)
        {
            speed = 3f;
            this.GetComponent<Animation>().Play("ZombieWalk");
            transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.fixedDeltaTime);
            transform.LookAt(target);              
        }
        if(distance > FollowRadius)
        {
            transform.position = Vector3.MoveTowards(transform.position, target.position, wait * Time.fixedDeltaTime);
            if(distance <= hearRadius)
            {         
                transform.LookAt(target);
            }
        }
        if(distance <= attackRadius)
        {
            speed = wait;
            this.GetComponent<Animation>().Play("ZombieAttack");
            StartCoroutine(InflictDamage());
        }
    }
    IEnumerator InflictDamage()
    {
        isAttacking = true;
        yield return new WaitForSeconds(1.1f);
        GlobalHealth.currentHealth -= 0.1f;
        yield return new WaitForSeconds(1f);
        isAttacking = false;
    }
}

  
