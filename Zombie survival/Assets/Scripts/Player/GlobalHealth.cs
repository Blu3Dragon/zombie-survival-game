﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GlobalHealth : MonoBehaviour
{
    public static float currentHealth = 20;
    public float internalHealth;

    // Update is called once per frame

    private void Start()
    {
        currentHealth = 20;
    }
    void Update()
    {
        internalHealth = currentHealth;
        if (internalHealth < 0)
        {
            SceneManager.LoadScene(4);
        }
    }
    
}
