﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeapnCall : MonoBehaviour
{
    public float TheDistance;
    public GameObject ActionDisplay;
    public GameObject ActionText;
    public GameObject TheWeapon;
    public GameObject Arrow;
    public AudioSource CollectSound;
    public GameObject WearWeapon;
    public GameObject Ammo;

    // Update is called once per frame
    void Update()
    {
        TheDistance = PlayerAction.DistanceFromTarget;
    }
    void OnMouseOver()
    {
        if(TheDistance <= 3)
        {
            ActionDisplay.SetActive(true);
            ActionText.SetActive(true);
        }
        if (Input.GetButtonDown("Action"))
        {
            if(TheDistance <= 3)
            {
                this.GetComponent<MeshRenderer>().enabled = false;
                //this.GetComponent<MeshCollider>().enabled = false;
                this.GetComponent<BoxCollider>().enabled = false;
                WearWeapon.SetActive(true);
                ActionDisplay.SetActive(false);
                ActionText.SetActive(false);
                CollectSound.Play();
                Arrow.SetActive(false);
                Ammo.SetActive(true);
               
            }
        }
    }
    void OnMouseExit()
    {
        ActionDisplay.SetActive(false);
        ActionText.SetActive(false);
    }
}
