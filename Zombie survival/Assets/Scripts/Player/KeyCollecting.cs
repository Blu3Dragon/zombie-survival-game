﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyCollecting : MonoBehaviour
{
    float speed = 50.0f;
    public GameObject ThePlayer;
    public GameObject TextBox;
    public GameObject HospitalKey;
    public GameObject LightKey;
    public GameObject ParticleSystem;
    public GameObject WinCondition;


    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.up * speed * Time.deltaTime);
    }

    void OnTriggerEnter(Collider other)
    {
        StartCoroutine(ScenePlayer());
    }

    IEnumerator ScenePlayer()
    {
        
        TextBox.GetComponent<Text>().text = "I've got the key!";
        yield return new WaitForSeconds(1.5f);
        TextBox.GetComponent<Text>().text = "";
        HospitalKey.SetActive(false);
        LightKey.SetActive(false);
        ParticleSystem.SetActive(false);
        WinCondition.SetActive(true);
    }
}
