﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class StoryLine09 : MonoBehaviour
{
    public GameObject ThePlayer;
    public GameObject TextBox;
    public GameObject Cube;
    public GameObject Zombie;

    private void OnTriggerEnter(Collider other)
    {
        Zombie.SetActive(true);
        StartCoroutine(ScenePlayer());
    }

    IEnumerator ScenePlayer()
    {
        TextBox.GetComponent<Text>().text = "The Zombie boss! I need to kill it!";
        yield return new WaitForSeconds(2f);
        TextBox.GetComponent<Text>().text = "";
        Cube.GetComponent<BoxCollider>().enabled = false;
        
    }
}
