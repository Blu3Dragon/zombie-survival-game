﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class StoryLine01 : MonoBehaviour
{

    public GameObject ThePlayer;
    public GameObject FadeScreenIn;
   // public GameObject TextBox;
    public GameObject MainMessage;
    float wait = 0f;
    float OldSpeed;
    void Start()
    {
        OldSpeed = ThePlayer.GetComponent<PlayerMovement>().speed;
        ThePlayer.GetComponent<PlayerMovement>().speed = wait;
        StartCoroutine(ScenePlayer());
    }
        IEnumerator ScenePlayer ()
        {
        yield return new WaitForSeconds(1.5f);
        FadeScreenIn.SetActive(false);
       // TextBox.GetComponent<Text>().text = "I need to get out of here!";
        MainMessage.GetComponent<Text>().text = "Find way to Hospital and don't get killed.";
        yield return new WaitForSeconds(4);
        MainMessage.GetComponent<Text>().text = "";
        ThePlayer.GetComponent<PlayerMovement>().speed = OldSpeed;
        }
}
