﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoryLine05 : MonoBehaviour
{
    public GameObject ThePlayer;
    public GameObject TextBox;
    public GameObject Cube;

    private void OnTriggerEnter(Collider other)
    {
        StartCoroutine(ScenePlayer());
    }

    IEnumerator ScenePlayer()
    {
        TextBox.GetComponent<Text>().text = "I can see the Hospital!";
        yield return new WaitForSeconds(2f);
        TextBox.GetComponent<Text>().text = "";
        Cube.GetComponent<BoxCollider>().enabled = false;
    }
}
