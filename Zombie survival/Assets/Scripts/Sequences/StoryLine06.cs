﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoryLine06 : MonoBehaviour
{
    public GameObject ThePlayer;
    public GameObject TextBox;
    public GameObject Cube;
    public GameObject Arrow;
    public GameObject GateCube;
    public GameObject GateLight;
   // public GameObject 
    float wait = 0f;
    float OldSpeed;

    private void OnTriggerEnter(Collider other)
    {
        OldSpeed = ThePlayer.GetComponent<PlayerMovement>().speed;
        //ThePlayer.GetComponent<CharacterController>().enabled = false;
        ThePlayer.GetComponent<PlayerMovement>().speed = wait;
        StartCoroutine(ScenePlayer());
    }

    IEnumerator ScenePlayer()
    {
        TextBox.GetComponent<Text>().text = "Damn it, I need a key!";
        yield return new WaitForSeconds(2f);
        TextBox.GetComponent<Text>().text = "";
        ThePlayer.GetComponent<PlayerMovement>().speed = OldSpeed;
        //ThePlayer.GetComponent<CharacterController>().enabled = true;
        Cube.GetComponent<BoxCollider>().enabled = false;
        Arrow.SetActive(true);
        GateCube.SetActive(true);
        GateLight.SetActive(true);
    }
}
