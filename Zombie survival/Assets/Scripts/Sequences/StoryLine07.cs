﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoryLine07 : MonoBehaviour
{
    public GameObject ThePlayer;
    public GameObject TextBox;
    public GameObject Cube;
    public GameObject Gate;
    public GameObject Arrow;
    public GameObject Light;
    float wait = 0f;
    float OldSpeed;

    private void OnTriggerEnter(Collider other)
    {
        OldSpeed = ThePlayer.GetComponent<PlayerMovement>().speed;
        //ThePlayer.GetComponent<CharacterController>().enabled = false;
        ThePlayer.GetComponent<PlayerMovement>().speed = wait;
        StartCoroutine(ScenePlayer());
    }

    IEnumerator ScenePlayer()
    {
        TextBox.GetComponent<Text>().text = "The key is at the end of this road.";
        yield return new WaitForSeconds(3f);
        TextBox.GetComponent<Text>().text = "";
        ThePlayer.GetComponent<PlayerMovement>().speed = OldSpeed;
        //ThePlayer.GetComponent<CharacterController>().enabled = true;
        Cube.GetComponent<BoxCollider>().enabled = false;
        Gate.SetActive(false);
        Arrow.SetActive(false);
        Light.SetActive(false);
    }
}
