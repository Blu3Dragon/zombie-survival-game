﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Arrow01Trigger : MonoBehaviour
{
    public GameObject Player;
    public GameObject TextBox;
    public GameObject Marker;

    void OnTriggerEnter()
    {
        Player.GetComponent<CharacterController>().enabled = false;
        StartCoroutine(ScenePlayer());
    }
    IEnumerator ScenePlayer()
    {
        Player.GetComponent<Text>().text = "It looks like a weapon right here";
        yield return new WaitForSeconds(2.5f);
        TextBox.GetComponent<Text>().text = "";
        Player.GetComponent<CharacterController>().enabled = true;
        Marker.SetActive(true);
    }
}
