﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreepyIntensityLight : MonoBehaviour
{
    public float minIntensity;
    public float maxIntensity;
    Light clight;
    float random;

    // Start is called before the first frame update
    void Start()
    {
        clight = GetComponent<Light>();
        random = Random.Range(0.0f, 65535.0f);
    }

    // Update is called once per frame
    void Update()
    {
        float noise = Mathf.PerlinNoise(random, Time.time);
        clight.intensity = Mathf.Lerp(minIntensity, maxIntensity, noise);
    }
}
